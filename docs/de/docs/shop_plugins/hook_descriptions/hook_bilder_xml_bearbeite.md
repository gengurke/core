# HOOK_BILDER_XML_BEARBEITE (138)

## Triggerpunkt

Am Anfang der Bildbearbeitung, in dbeS

## Parameter

* `string` **Pfad** - Pfad zu den ausgepackten Bildern
* `array|stdClass` **&Artikel** - Artikelbilder
* `array|stdClass` **&Kategorie** - Kategoriebilder
* `array|stdClass` **&Eigenschaftswert** - Eigenschaftsbilder
* `array|stdClass` **&Hersteller** - Herstellerbilder
* `array|stdClass` **&Merkmalwert** - Merkmalwertbilder
* `aray|stdClass` **&Merkmal** - Merkmalbilder
* `array|stdClass` **&Konfiggruppe** - Konfigurationsgruppenbilder