# HOOK_CONFIG_ITEM_GETPREIS

## Triggerpunkt

In `JTL\Extensions\Config\Item::getPreis` nach der Preisberechnung eines Konfig-Items.

## Parameter

* `JTL\Extensions\Config\Item` **configItem** - das Konfig-Item selbst
* `float` **&fVKPreis** - der berechnete Preis