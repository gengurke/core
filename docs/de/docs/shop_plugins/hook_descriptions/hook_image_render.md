# HOOK_IMAGE_RENDER (216)

## Triggerpunkt

Vor dem Speichern eines neu angepaßten Bildes

## Parameter

* `\Intervention\Image\Image` **image** - Bildobjekt
* `array` **settings** - Einstellungen für das Rendern
* `string` **thumbnail** - Pfad zum Thumbnail