# HOOK_IO_HANDLE_REQUEST_ADMIN (311)

## Triggerpunkt

Nachdem Callback-Methoden definiert wurden und bevor der Request bearbeitet wird

## Parameter

* `\JTL\Backend\AdminIO` **&io** - IO-Objekt
* `string` **&request** - Request-String
