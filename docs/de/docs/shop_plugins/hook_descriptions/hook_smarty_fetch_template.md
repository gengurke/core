# HOOK_SMARTY_FETCH_TEMPLATE (208)

## Triggerpunkt

Vor dem Laden des Templates

## Parameter

* `string` **&original** - orginaler Templatename
* `string` **&custom** - custom-Templatename
* `string` **&fallback** - fallback custom-Templatename
* `string` **&out** - zu nutzender Templatename
* `bool` **transform** - (ab Version 5.0) Flag für "transform file to string"