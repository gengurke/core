# HOOK_SYNC_ADDED_PRODUCT

## Triggerpunkt

In ``JTL\dbeS\Sync\Products::handleInserts`` nach Erstellen des Produktes.

## Parameter

* `int` **productID** - die ID des Artikels
* `stdClass` **product** - Das Produktobjekt
* `array` **xml** - Das von der Wawi gesendete XML