# Besonderheiten im Zahlungs-Plugin JTL PayPal Checkout

## Template-Selectoren (JTL PayPal Checkout)

Nachfolgende Selectoren werden im "*JTL PayPal Checkout*"-Plugin verwendet. 
Bitte stellen Sie sicher, dass diese Selektoren im Template enthalten sind und die adäquaten Bereiche
wie im NOVA-Template referenzieren, um eine korrekte Funktion des "JTL PayPal Checkout"-Plugins zu gewährleisten.

Selectoren in der: **CheckoutPage.php** (phpQuery)


    - \*_phpqSelector
    - #complete-order-button
    - body
    - .checkout-payment-method
    - .checkout-shipping-form
    - #fieldset-payment
    - #result-wrapper
    - meta[itemprop="price"]

Selectoren in der: **CheckoutPage.php** (phpQuery)


    - #miniCart-ppc-paypal-standalone-button
    - #cart-ppc-paypal-standalone-button
    - #\*-ppc-\*-standalone-button
    - #productDetails-ppc-paypal-standalone-button
    - #cart-checkout-btn
    - #add-to-cart button[name="inWarenkorb"]
    - meta[itemprop="price"]
    - #buy_form
    - #complete-order-button
    - #paypal-button-container
    - #complete_order
    - #comment
    - #comment-hidden
    - form#complete_order
    - .checkout-payment-method
    - #za_ppc_\*_input
    - input[type=radio][name=Zahlungsart]
    - #fieldset-payment .jtl-spinner

## Debug-ID ermitteln bei Onboarding-Fehlern

Ggfs. kommt es vor, dass beim automatischen Onboarding Fehler mit nichtssagenden Meldungen ("Etwas ist schiefgelaufen")
auftreten und das Onboarding dadurch nicht abgeschlossen werden kann. Der normale PayPal-Support kann in solchen
Fällen meist nicht weiterhelfen und es müssen Tickets beim Tec-Support eröffnet werden, die eine PayPal-Debug-ID
erfordern. Nachfolgend wird beschrieben, wie Sie als Entwickler, Servicepartner oder technisch versierter Anwender diese ID
ermitteln können.

Bevor Sie den Onboarding-Vorgang erneut starten, überprüfen Sie bitte die PayPal-Status-Seite auf aktuell offene Probleme.
Gehen Sie dazu auf https://www.paypal-status.com/product/production und prüfen Sie diese auf aktuelle Einschränckungen.
Prüfen Sie auch den JTL-Issue-Tracker unter https://issues.jtl-software.de/issues?project=shop&component=11101&type=bug&status=open auf offene
Fehler für das JTL-PayPal-Plugin, die das Onboarding betreffen. 
Wenn auf der PayPal-Status-Seite keine Probleme gelistet werden und auch keine diesbezgl. Fehler im JTL-IssueTracker
zu finden sind, können Sie folgendermassen vorgehen:

- Starten Sie das PayPal-Onboarding im Plugin-Backend
- Aktivieren Sie die Entwickler-Tools des Browsers (Rechtsklick mit der Maus innerhalb des Minibrowser-Fensters und dann auf "Untersuchen" klicken)
- Führen Sie das Onboarding bis zu der Stelle aus, an der der Fehler angezeigt wird
- Klicken Sie in den Entwickler-Tools den Tab "Inspektor" (Firefox) bzw. "Elements" (Chrome) an
- Klicken Sie im angezeigten DOM-Baum auf das oberste Element (<html...) und starten Sie die Suche (Strg+F)
- Tippen Sie "debug" in die Sucheingabe und durchsuchen Sie den DOM-Baum nach einem (oder mehreren) Treffer für "debug-id" oder auch "debug_id"
- Die PayPal-Debug-ID ist ein hexadezimal-Wert mit typischwerweise 13 Stellen (dies kann jedoch variieren)
- Eine PayPal-Debug-ID sieht z.B.: so aus: 8f5bd2e03a959
- Notieren Sie sich die gefundene(n) Debug-ID(s)

Unter Angabe Ihrer Merchant-ID (diese finden Sie in Ihrem Kunden-Account bei PayPal), der genauen Zeit des Onboardings,
der angezeigten Fehlermeldung (oder des Fehlerzustandes) und der ermittelten PayPal-Debug-ID(s) können Sie sich an den
PayPal-Support wenden. Ggfs. bitten Sie dort gleich um Weiterleitung an den Tec-Support von PayPal. 
Sollte Ihnen der PayPal-Support nicht weiterhelfen können, eröffnen Sie bitte mit denselben
Informationen zzgl. der Angabe Ihrer PayPal Support Ticket-ID / Vorgangsnummer und dem Namen des PayPal-Mitarbeiters,
ein Ticket beim Support von JTL. Wir werden die Informationen dann unsererseits an PayPal weitergeben.