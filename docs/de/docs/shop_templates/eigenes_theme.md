# Eigenes Theme

## Was ist ein Theme?

Ein Theme ist kein eigenständiges Template, sondern ausschließlich die Design-Komponente, die zu einem Template
gehört. In JTL-Shop steuern Themes das individuelle Aussehen und Design des Shops per CSS-Regeln. Das Template stellt
das XHTML-Ausgabedokument bereit, während das Theme die einzelnen Elemente des XHTML-Ausgabedokumentes per Cascading
Stylesheets (CSS) visuell für die Bildschirm- und Druckausgabe anpasst.

!!! important
   Wie auch für Ihr eigenes Template, gilt für ein eigenes Theme: kopieren Sie sich das Beispiel-Theme "my-nova" aus
   dem Verzeichnis `themes/` in ihrem [Child-Template](https://build.jtl-shop.de/#template) und benennen dieses Ihren
   Wünschen gemäß um.

## Struktur eines Themes

Ein *Theme* besteht aus einem Verzeichnis, welches sich unterhalb von `[Shop-Root]/templates/[Template-Name]/themes/`
befindet, sowie aus der darin enthaltenen CSS-Datei `custom.css` und den SASS-Dateien `sass/_variables.scss` sowie
`sass/[Theme-Name].scss` und `sass/[Theme-Name]_crit.scss`.

**Beispiel:**

``` hl_lines="7-18"
templates/NOVAChild/
├── account/
├── ...
├── themes/
│   ├── base/
│   │   └── ...
│   ├── my-nova/
│   │   ├── images/
│   │   │   ├── header-background.jpg
│   │   │   └── ...
│   │   ├── sass/
│   │   │   ├── _variables.scss
│   │   │   └── my-nova.scss
│   │   │   └── my-nova_crit.scss
│   │   ├── custom.css
│   │   ├── my-nova.css
│   │   ├── my-nova.css.map
│   │   └── my-nova_crit.css
│   └─ ...
```

### variables.less / _variables.scss

Diese Datei beinhaltet vordefinierte Variablen mit Farbwerten, Abständen, Breiten etc.

### my-nova.scss

In dieser Datei werden das Aussehen und das Design des Shops beeinflusst. Dabei kann auf die Variablen der
`variables.less` bzw. der `_variables.scss` zurückgegriffen werden.

!!! attention
    Die Pfade zum Parent-Template müssen in Ihrer `my-nova.scss` korrekt definiert sein.

**Beispiel:**

    // Load core functions
    @import '../../../../NOVA/themes/base/sass/functions';

    // Load style definitions from NOVA clear theme
    @import '../../../../NOVA/themes/clear/sass/allstyles';

## CSS Dateien anpassen

Sie können, neben dem [Ändern und Erweitern](./eigenes_template.md) von Template-Dateien, auch das CSS des Templates
erweitern oder überschreiben.

Um Ihre eigenen CSS Dateien in Ihrem Child-Template zu laden, gehen Sie bitte in die `template.xml` Ihres
Child-Templates. Dort finden Sie den Abschnitt `<Minify>`, in dem Sie Ihre Dateien eintragen können.

Sie können auch mehrere Themes anlegen, z.B. ein Weihnachts- und ein Oster-Theme, indem Sie einfach mehrere `<CSS>`
Blöcke anlegen. Dabei sollten Sie beachten, dass für jeden CSS Block ein Ordner mit gleichem Nahem in
`[Shop-Root]/templates/[Template-Name]/themes/` angelegt werden muss. Dieser Ordner darf leer sein, wenn Sie z.B. ihre
CSS Dateien von einem anderen Ort laden.

``` hl_lines="18-29"
<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<Template isFullResponsive="true">
    <Name>NOVAChild</Name>
    <Author>Max Mustermann</Author>
    <URL>https://www.mein-shop.de</URL>
    <Parent>NOVA</Parent>
    <Preview>preview.png</Preview>
    <Description>Dieses Template dient als Vorlage für ein eigenes Child-Template des NOVA.</Description>

    <Settings>
        <Section Name="Theme" Key="theme">
            <Setting Description="Mein Theme" Key="theme_default" Type="select" Value="my-nova">
                <Option Value="my-nova">Mein erstes Theme</Option>
            </Setting>
        </Section>
    </Settings>
    <Minify>
        <CSS Name="weihnachtstheme.css">
           <File Path="../NOVA/themes/blackline/blackline.css"/>
           <File Path="../NOVA/themes/blackline/blackline_crit.css"/>
           <File Path="themes/weihnachtstheme/weihnachtstheme.css"/>
       </CSS>
        <CSS Name="ostertheme.css">
           <File Path="themes/ostertheme/ostertheme.css"/>
       </CSS>
       <CSS Name="my-nova.css">
           <File Path="themes/my-nova/my-nova.css"/>
       </CSS>
    </Minify>
    <Boxes>
        <Container Position="right" Available="1"></Container>
    </Boxes>
</Template>
```

Selbstverständlich können Sie die Dateien auch benennen, wie Sie möchten, solange Sie Ihre Referenzierung
dementsprechend anpassen.

Wenn Sie unserem Beispiel gefolgt sind, müsste demnach Ihr Child-Template mittlerweile so aussehen:

    templates/NOVAChild/
    ├── js/
    │   └── custom.js
    ├── ...
    ├── themes/
    │   ├── base/
    │   │   └── ...
    │   ├── my-nova/
    │   │   ├── sass/
    │   │   │   ├── _variables.scss
    │   │   │   └── my-nova.scss
    │   │   │   └── my-nova_crit.scss
    │   │   ├── my-nova.css
    │   │   ├── my-nova.css.map
    │   │   └── my-nova_crit.css
    │   ├── weihnachtstheme/
    │   │   ├── weihnachtstheme.css
    │   ├── ostertheme/
    │   │   ├── ostertheme.css
    ├── template.xml
    ├── preview.png
    └── ...

## JavaScript Dateien anpassen

Eigene JS Dateien können Sie direkt im Footer oder Head ihrer Webseite einbinden. Je nachdem ob das Script vor oder
dem Seitenaufbau geladen werden soll. |br|
Für eine optimale Performance, sollten Sie Ihre JS Dateien nicht im Head, sondern im Footer laden und mit dem async
oder defer Attribut versehen.

!!! note
    Das async Attribut lädt die Datei asynchron, d.h. der Seitenaufbau wird nicht blockiert. Das defer Attribut lädt
    die Datei ebenfalls asynchron, jedoch wird die Datei erst ausgeführt, wenn der Seitenaufbau abgeschlossen ist.

!!! attention
   Die Datei `js/custom.js` wird automatisch in den Footer geladen und sollte deshalb nicht noch einmal in der
   `template.xml` referenziert werden.

Wenn Sie z.B. die Datei `js/onload.js` im Footer laden möchten, könnten Sie diese wie folgt einbinden:

1. Erstellen Sie die Datei `js/onload.js` in Ihrem Child-Template Ordner.
2. Erstellen Sie den Ordner `layout` in Ihrem Child-Template Ordner.
3. Erstellen Sie die Datei `layout/footer.tpl` in Ihrem Child-Template Ordner.
4. Fügen Sie folgenden Code in die `layout/footer.tpl` ein:

```
{block name='layout-footer-js' append}
    <script defer src="{$ShopURL}/{$currentTemplateDir}js/onload.js?v={$nTemplateVersion}"></script>
{/block}
```

Als Alternative können Sie auch die `template.xml` Ihres Child-Templates anpassen:

``` hl_lines="29-31"
<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<Template isFullResponsive="true">
    <Name>NOVAChild</Name>
    <Author>Max Mustermann</Author>
    <URL>https://www.mein-shop.de</URL>
    <Parent>NOVA</Parent>
    <Preview>preview.png</Preview>
    <Description>Dieses Template dient als Vorlage für ein eigenes Child-Template des NOVA.</Description>

    <Settings>
        <Section Name="Theme" Key="theme">
            <Setting Description="Mein Theme" Key="theme_default" Type="select" Value="my-nova">
                <Option Value="my-nova">Mein erstes Theme</Option>
            </Setting>
        </Section>
    </Settings>
    <Minify>
        <CSS Name="weihnachtstheme.css">
           <File Path="../NOVA/themes/blackline/blackline.css"/>
           <File Path="../NOVA/themes/blackline/blackline_crit.css"/>
           <File Path="themes/weihnachtstheme/weihnachtstheme.css"/>
       </CSS>
        <CSS Name="ostertheme.css">
           <File Path="themes/ostertheme/ostertheme.css"/>
       </CSS>
       <CSS Name="my-nova.css">
           <File Path="themes/my-nova/my-nova.css"/>
       </CSS>
       <JS Name="jtl3.js">
           <File Path="js/onload.js"/>
       </JS>
    </Minify>
    <Boxes>
        <Container Position="right" Available="1"></Container>
    </Boxes>
</Template>
```

!!! attention
   Bitte beachten Sie, dass JavaScript Dateien welche Sie in der `template.xml` referenzieren, im HEAD Bereich ihrer
   Webseite geladen werden und sich somit negativ auf die Ladezeit ihres Shops auswirken können.

## Arbeiten mit SASS

Das NOVA-Template arbeitet mit SASS-Dateien. SASS kann als sprachliche Erweiterung von CSS verstanden werden und bietet
gegenüber alleinigem CSS einige Vorteile. So können CSS-Angaben beispielsweise verschachtelt und wiederverwendet
werden. Dadurch können Sie Ihre Styles besser und übersichtlicher strukturieren.

!!! note
   SASS setzt einen Pre-Prozessor voraus, welcher die Sprachkonstrukte von SASS in CSS übersetzt. Dieser Pre-Processor
   wird über das JTL-Plugin [JTL Theme-Editor](https://build.jtl-shop.de/#plugin) bereitgestellt.

Hier sehen Sie den Unterschied zwischen CSS und SASS:

**CSS**

    header {
        padding: 5px;
    }

    header #header-branding {
        padding: 15px 0;
    }

**SASS**

    header {
        padding: 5px;
        #header-branding {
            padding: 25px;
        }
    }

Weitere Informationen dazu, was *SASS* Ihnen bieten kann, finden Sie auf [sass-lang.com](https://sass-lang.com/).

## Eigene SASS-Dateien im Theme

Wenn Sie in Ihrem Child-Template auch mit SASS arbeiten möchten, empfiehlt es sich, den Ordner `my-nova/` aus dem
`themes/`-Order des NOVAChild-Templates zu kopieren und entsprechend umzubenennen, z.B. in `meinSassTheme/`.

``` hl_lines="7-10"
templates/NOVAChild/
├── ...
├── themes/
│   ├── base/
│   │   └── ...
│   ├── meinSassTheme/
│   │   ├── sass/
│   │   │   ├── _variables.scss
│   │   │   └── meinSassTheme.scss
│   │   │   └── meinSassTheme_crit.scss
│   │   ├── meinSassTheme.css
│   │   ├── meinSassTheme.css.map
│   │   └── meinSassTheme_crit.css
├── template.xml
├── preview.png
└── ...
```

Sie können nun in Ihrer `meinSassTheme.scss` SASS- oder CSS-Code einfügen und Ihren Shop individuell gestalten. Wenn
Sie Variablen in der Datei `variables.scss` ändern, werden diese für alle Styles in Ihrem Shop geändert. Sie können
z. B. die Variable `$primary` verändern und eine eigene Farbe eintragen. `$primary: #f800be;` wird für viele Elemente
in JTL-Shop verwendet. Das Ändern dieser Variable hat also starken Einfluss auf das Aussehen von JTL-Shop. Probieren
Sie es aus!

**Anschließend müssen Sie Ihr Theme noch kompilieren!**
(siehe [Eigenes Theme mit dem Theme-Editor kompilieren](#eigenes-theme-mit-dem-theme-editor-kompilieren))

!!! note
    SASS-Dateien müssen **nicht** in die `template.xml` eingefügt werden. Der Theme-Editor erkennt SASS-Files
    automatisch.

## Eigenes Theme mit dem Theme-Editor kompilieren

Gehen Sie hierfür in das Backend von JTL-Shop. Falls noch nicht geschehen, aktivieren Sie das Plugin
"**JTL Theme-Editor**". Anschließend öffnen Sie das Plugin über seinen "Einstellungen"-Button in der Pluginverwaltung.
Wählen Sie nun in der Select-Box unter Theme Ihr Theme aus und klicken anschließend rechts auf den Button
`Theme kompilieren`.

![](_images/jtl-shop_child-template_editor.jpg)

Nun ist Ihr Template fertig kompiliert.

!!! important
    Ihr `theme`-Ordner benötigt Schreibrechte.

Weitere Möglichkeiten um Ihr Theme zu bearbeiten, finden Sie im Abschnitt [Themes editieren](theme_edit.md).

## Updatesicherheit

Um sicherzugehen, dass Ihre Änderungen in der Datei `template.xml` nicht durch ein Update rückgängig gemacht werden,
empfehlen wir, das eigene Theme in einem [Child-Template](./eigenes_template.md) abzulegen.